﻿using NUnit.Framework;
using pankki;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pankkiTests
{
    [TestFixture]
    public class PankkitiliTest
    {

        /* Pankki sisältää:
         * Luokka, jonka nimi on Pankkitili.
         * Pankkitilissä on kolme toimintoa:
         * - tallettaa rahaa
         * - nostaa rahaa
         * -- tilillä pitää olla tietty määrä rahaa ettei mene miinukselle
         * - siirtää rahaa tililtä toiselle
         * -- edelleen pitää olla rahaa ettei miinukselle
         */

        public Pankkitili tili1 = null;

        // OneTimeSetUp
        // OneTimeTearDown
        // TearDown
        [SetUp]
        public void TestienAlustaja()
        {
            this.tili1 = new Pankkitili(100);
        }

        [Test]
        public void LuoPankkitili()
        {
            // Testataan olion luokan tyyppi
            Assert.IsInstanceOf<Pankkitili>(tili1);
        }

        [Test]
        public void AsetaPankkitililleAlkusaldo()
        {
            // Testataan arvon yhtäsuuruutta
            Assert.That(100, Is.EqualTo(tili1.Saldo));
        }

        [Test]
        public void TalletaRahaaPankkitilille()
        {
            // Talletetaan rahaa tilille
            tili1.Talleta(250);

            // Testataan arvon yhtäsuuruutta
            Assert.That(350, Is.EqualTo(tili1.Saldo));
        }

        [Test]
        public void NostaPankkitililtaRahaa()
        {
            // Nostetaan tililtä rahaa
            tili1.NostaRahaa(75);

            // Testataan arvon yhtäsuuruutta
            Assert.That(25, Is.EqualTo(tili1.Saldo));
        }

        [Test]
        public void NostonJalkeenPankkitiliEiVoiOllaMiinuksella()
        {
            // Testataan antaako ohjelma halutun virhetyypin
            Assert.Throws<ArgumentException>(() => tili1.NostaRahaa(175));

            // Vaikka virhe sattuu niin rahoja ei menetetä.
            // Pitäisi olla loppusaldon kanssa sama.
            Assert.That(100, Is.EqualTo(tili1.Saldo));
        }
    }
}
